<!DOCTYPE html>
<html lang="nl" class="nojs">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>MAJOR_DOK</title>
    <meta name="robots" content="noindex, nofollow">
    <meta name="description" content="/">
    <meta name="keywords" content="/">
    <meta name="author" content="Jasper Van Gestel">
    <script>
      var html = document.documentElement || document.getElementsByClassName(`html`)[0];
      html.classList.remove(`nojs`);
      html.classList.add(`js`);
    </script>
    <script>
      WebFontConfig = {
        google: {
          families: [`Lora:400,700i`]
        },
        custom: {
          families: [`Pressura:400,700i`],
          urls: [`<?php echo ROOT ?>assets/fonts/pressura/pressura.css`]
        }
      };
      (function(d) {
        var wf = d.createElement(`script`), s = d.scripts[0];
        wf.src = `https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js`;
        s.parentNode.insertBefore(wf, s);
      })(document);
    </script>
    <?php echo $css ?>
  </head>
  <body>
  	<?php
      if (isset($_SESSION['messages'])) {
        echo '<div class="messages">';
        foreach ($_SESSION['messages'] as $message) {
          echo '<div class="message message_type_' . $message['type'] . '">';
            echo '<span class="message__text">' . $message['text'] . '</span>';
            echo '<span class="message__close">&times;</span>';
          echo '</div>';
        }
        echo '</div>';
        unset($_SESSION['messages']);
      }
      ?>
      <?php
      echo '<div class="page-wrapper">' . $content . '</div>';
      echo $js;
    ?>
  </body>
</html>
