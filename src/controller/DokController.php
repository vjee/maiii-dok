<?php
	require_once WWW_ROOT . 'controller' . DS . 'Controller.php';
	require_once WWW_ROOT . 'dao' . DS . 'EventDAO.php';
	require_once WWW_ROOT . 'dao' . DS . 'BlogDAO.php';
	require_once WWW_ROOT . 'dao' . DS . 'ZoneDAO.php';
	require_once WWW_ROOT . 'dao' . DS . 'NewsletterDAO.php';

	class DokController extends Controller {

		protected $eventDAO;
		protected $blogDAO;
		protected $zoneDAO;
		protected $newsletterDAO;
		function __construct() {
			$this->eventDAO = new EventDAO();
			$this->blogDAO = new BlogDAO();
			$this->zoneDAO = new ZoneDAO();
			$this->newsletterDAO = new NewsletterDAO();
		}

		private function getFilterResults($conditions) {
			$returnVal = [];
			$events = $this->eventDAO->search($conditions);
			foreach ($events as $event) {
				$id = $event['id'];
				$evt_id = $event['event_id'];
				$DBimage = $this->eventDAO->getEventThumbById($evt_id)['url'];
				$DBlocations = $this->eventDAO->getEventLocationsById($id);
				$combined = [
					'id' => $event['id'],
					'title' => $event['title'],
					'preview' => $event['preview'],
					'start' => date('d-m', strtotime($event['start'])),
					'end' => date('d-m', strtotime($event['end'])),
					'locations' => $DBlocations,
					'image' => $DBimage
				];
				$returnVal[] = $combined;
			}
			return $returnVal;
		}

		private function filterByName($name) {
			$conditions = array(
				array('field' => 'title', 'comparator' => 'like', 'value' => $name)
			);
			$events = $this->getFilterResults($conditions);
	    $this->set('events', array(
				'events' => $events,
				'filter' => '\'' . $name . '\''
			));
		}

		private function filterByMonth($start, $end, $month, $originalValue) {
			$conditions = array(
	    	array('field' => 'end', 'comparator' => '>=', 'value' => $start),
    		array('field' => 'end', 'comparator' => '<', 'value' => $end)
			);
			$events = $this->getFilterResults($conditions);
	    $this->set('events', array(
				'events' => $events,
				'filter' => $month
			));
		}

		// set session message
		private function message($text, $type) {
			$_SESSION['messages'][] = ['text' => $text, 'type' => $type];
		}

		// get 2 most viewed events
		public function home() {
			// events
			$returnVal = [];
			$DBevents = $this->eventDAO->selectTopTwoEvents();
			foreach ($DBevents as $event) {
				$DBimage = $this->eventDAO->getEventThumbById($event['event_id'])['url'];
				$DBlocations = $this->eventDAO->getEventLocationsById($event['id']);
				$returnVal[] = array_merge($event, array(
					'image' => $DBimage,
					'locations' => $DBlocations,
				));
			}

			// blog posts
			$blogPosts = $this->blogDAO->selectPosts(0, 3);

			// zones
			$zones = $this->zoneDAO->selectZones(0, 3);

			// push to frontend
			$this->set('top_events', $returnVal);
			$this->set('blog_posts', $blogPosts);
			$this->set('zones', $zones);
		}

		private function getExtraData($events, $rowCount) {
			$returnVal = [
				'events' => [],
				'eventCount' => $rowCount['count']
			];
			foreach ($events as $event) {
				$DBimage = $this->eventDAO->getEventThumbById($event['event_id'])['url'];
				$DBlocations = $this->eventDAO->getEventLocationsById($event['id']);
				$returnVal['events'][] = array_merge($event, array(
					'locations' => $DBlocations,
					'image' => $DBimage
				));
			}
			return $returnVal;
		}

		// get first 10 events
		public function eventlist() {
			// filter by month
			if (isset($_POST['month'])) {
				if ($_POST['month'] !== '-1') {
					$months = ['januarie', 'februarie', 'maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september', 'october', 'november', 'december'];
					$val = (int)$_POST['month'];
					$start = '2017-' . $val . '-01 00:00:00';
					$end = '2017-' . ($val + 1) . '-01 00:00:00';
					$month = $months[($val - 1)];
					$this->filterByMonth($start, $end, $month, $_POST['month']);
				} else {
					$this->message('Je hebt geen maand geselecteerd om naar te zoeken. Probeer nogmaals.', 'error');
					$this->redirect(ROOT . 'events');
				}
			}
			// filter by name
			else if (isset($_POST['name'])) {
				if ($this->isFilledIn($_POST['name'])) {
					$this->filterByName($_POST['name']);
				} else {
					$this->message('Je hebt geen event naam ingevuld om naar te zoeken. Probeer nogmaals.', 'error');
					$this->redirect(ROOT . 'events');
				}
			}
			// #nofilter
			else {
				$rowCount = $this->eventDAO->getRowCount();
				$DBevents;
				if (isset($_GET['items']) && $_GET['items'] === 'all') {
					// get all events (nojs fallback)
					$DBevents = $this->eventDAO->selectEvents(0, $rowCount['count']);
				} else {
					// get first 10 events
					$DBevents = $this->eventDAO->selectEvents(0, 10);
				}

				$returnVal = $this->getExtraData($DBevents, $rowCount);
				$this->set('events', $returnVal);
			}
		}

		// get complete event
		public function eventdetail() {
			if (isset($_GET['id']) && $_GET['id'] != '') {
				$id = (int)$_GET['id'];

				// get event
				$DBevent = $this->eventDAO->selectById($id);
				if ($DBevent != false || $DBevent != null || $DBevent != '') {
					$DBlocations = $this->eventDAO->getEventLocationsById($id);
					$DBimages = $this->eventDAO->getEventImagesById($DBevent['event_id']);
					$returnVal = []; $images = [];
					foreach ($DBimages as $image) {
						$images[] = $image['url'];
					}
					$returnVal = array_merge($DBevent, array(
						'locations' => $DBlocations,
						'images' => $images
					));

					// add views
					$views = (int)$DBevent['views'];
					$views = $views + 1;
					$this->eventDAO->addView($id, $views);

					// send data to frontend
					$this->set('event', $returnVal);
				} else {
					$this->redirect(ROOT . 'events');
				}
			} else {
				$this->redirect(ROOT . 'events');
			}
		}

		// email verification
		private function isValidEmail($email) {
			if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
				return true;
			}
			return false;
		}

		// string verification
		private function isFilledIn($value) {
			if ($value !== '') {
				return true;
			}
			return false;
		}

		// redirect to origin page
		private function redirectToOrigin($origin) {
			if (isset($origin)) {
				if ($origin == '' || $origin == '/' || $origin == 'home') {
					$this->redirect(ROOT);
				} else {
					$this->redirect(ROOT . $origin);
				}
			}
		}

		public function push() {
			if (isset($_GET['type']) && $_GET['type'] === 'newsletter' && isset($_POST['name']) && isset($_POST['email'])) {
				$error = false;

				// check for errors
				if (!$this->isValidEmail($_POST['email'])) {
					$error = true;
					$this->message('Je email adres is incorrect. probeer opnieuw.', 'error');
				}
				if (!$this->isFilledIn($_POST['name'])) {
					$error = true;
					$this->message('Je naam is niet ingevuld. probeer opnieuw.', 'error');
				}

				// push to server
				if ($error) {
					$this->redirectToOrigin($_GET['src']);
				} else {
					if ($this->newsletterDAO->addSubscriber($_POST['name'], $_POST['email'])) {
						// success
						$this->message('Bededankt voor je aan te melden. We houden je op de hoogte.', 'success');
					} else {
						// error
						$this->message('Oeps... Fout bij het aanmelden op de nieuwsbrief. Probeer nogmaals.', 'error');
					}
					$this->redirectToOrigin($_GET['src']);
				}
			} else {
				$this->redirect(ROOT);
			}
		}

		public function get() {

			// fetch extra events
			if (isset($_GET['type']) && $_GET['type'] === 'events' && isset($_GET['offset']) && $_GET['offset'] !== '' && isset($_GET['limit']) && $_GET['limit'] !== '') {
				if ($DBevents = $this->eventDAO->selectEvents($_GET['offset'], $_GET['limit'])) {
					$returnVal = [];
					foreach ($DBevents as $event) {
						$id = $event['id'];
						$evt_id = $event['event_id'];
						$DBimage = $this->eventDAO->getEventThumbById($evt_id)['url'];
						$DBlocations = $this->eventDAO->getEventLocationsById($id);
						$combined = [
							'root' => ROOT,
							'link' => ROOT .'events/' . $event['id'],
							'title' => $event['title'],
							'description' => $event['preview'],
							'start' => date('d-m', strtotime($event['start'])),
							'end' => date('d-m', strtotime($event['end'])),
							'locations' => $DBlocations,
							'image' => $DBimage
						];
						$returnVal[] = $combined;
					}
					if($this->isAjax) {
			      header('Content-Type: application/json');
			      echo json_encode(array('result' => 'ok', 'data' => $returnVal));
			      exit();
			    }
				} else {
					if($this->isAjax) {
						header('Content-Type: application/json');
						echo json_encode(array('result' => 'error'));
						exit();
					}
				}
			}
		}

	}
?>
