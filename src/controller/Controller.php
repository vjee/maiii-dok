<?php

class Controller {

  public $route;
  protected $viewVars = array();
  protected $isAjax = false;
  protected $env = 'development';

  public function filter() {
    //if we're running the project from a folder other than src, it means we're in production mode (dist)
    if(basename(dirname(dirname(__FILE__))) != 'src') {
      $this->env = 'production';
    }
    //if the request accept headers are set to json, we should send back json data instead of rendering html
    if(!empty($_SERVER['HTTP_ACCEPT']) && strtolower($_SERVER['HTTP_ACCEPT']) == 'application/json') {
      $this->isAjax = true;
    }
    //call the correct function in the controller
    call_user_func(array($this, $this->route['action']));
  }

  private function insertCriticalCSS($setCSS, $name) {
    if (file_exists('css/' . $name . '.critical.css')) {
      $css = file_get_contents('css/' . $name . '.critical.css');
      $setCSS .= '<style id="critical-css">' . $css . '</style>';
    }
    return $setCSS;
  }

  public function render($route = null) {
    $setCSS = '';
    $setJS = '';

    if ($route !== null) {
      $bundle = $route['bundle'];
      if($this->env == 'production') {
        // PRODUCTION
        if (isset($bundle['build']['css'])) {
          $cssLink = '<link rel="stylesheet" href="' . ROOT . 'css/' . $bundle['build']['css'] . '.css">';
          $setCSS .= '<noscript id="deferred-styles">' . $cssLink . '</noscript>';
          $setCSS = $this->insertCriticalCSS($setCSS, $route['name']);
          $setJS .= '<script>' . file_get_contents(WWW_ROOT . 'js' . DS . 'loadDefferedStyles.js') . '</script>';
        }
        if (isset($bundle['build']['js'])) {
          $setJS .= '<script async src="' . ROOT . 'js/' . $bundle['build']['js'] . '.js"></script>';
        }
      } else {
        // DEVELOPMENT
        if (isset($bundle['dev']['css'])) {
          $setCSS .= '<link rel="stylesheet" href="' . DEV_URL . 'css/' . $bundle['dev']['css'] . '.css">';
        }
        if (isset($bundle['dev']['js'])) {
          $setJS .= '<script src="' . DEV_URL . 'js/' . $bundle['dev']['js'] . '.js"></script>';
        }
      }
    }

    $this->set('css', $setCSS);
    $this->set('js', $setJS);

    $this->createViewVarWithContent();
    $this->renderInLayout();
    $this->cleanupSessionMessages();
  }

  public function set($variableName, $value) {
    $this->viewVars[$variableName] = $value;
  }

  public function redirect($url) {
    header("Location: {$url}");
    exit();
  }

  private function createViewVarWithContent() {
    extract($this->viewVars, EXTR_OVERWRITE);
    ob_start();
    require WWW_ROOT . 'view' . DS . strtolower($this->route['controller']) . DS . $this->route['action'] . '.php';
    $content = ob_get_clean();
    $this->set('content', $content);
  }

  private function renderInLayout() {
    extract($this->viewVars, EXTR_OVERWRITE);
    include WWW_ROOT . 'view' . DS . 'layout.php';
  }

  private function cleanupSessionMessages() {
    unset($_SESSION['info']);
    unset($_SESSION['error']);
  }

}
