<?php
	require_once WWW_ROOT . 'view' . DS . 'include' . DS . 'header.php';
	define('SRC', 'home');
?>
<main class="content home">


	<section class="intro">
		<h2 class="title title_type_big">wat is dok.</h2>
		<div class="intro__content">
			<div class="intro__content__text">
				<p class="paragraph">Op een imposant terrein naast het Handelsdok bij Gent Dampoort kiemt een
					nieuwe stadswijk. In afwachting van de uiteindelijke bouw van het project
					Oude Dokken is dit terrein een bruisende en gezellige ontmoetingsplek.
				</p>
				<p class="paragraph">De uitgelezen voedingsbodem voor kleine concerten, exposities, sport,
					terras, strand, film, wafelenbak, barbecue, frisse ideeën, levendige
					discussies, individuele gesprekken, piraterij en een vleugje waanzin.
				</p>
				<a href="/info" class="button button_color_red link_disabled">lees meer</a>
			</div>
			<div class="intro__content__image">
				<picture class="intro__content__picture">
					<source type="image/webp" srcset="<?php echo ROOT ?>assets/img/dok-intro_image.webp">
					<img src="<?php echo ROOT ?>assets/img/dok-intro_image.jpg" class="intro__content__picture" alt="intro foto">
				</picture>
			</div>
		</div>
	</section>


	<section class="insta">
		<h2 class="title title_type_big title_color_red"><span class="hidden">instagram </span>#dokgent</h2>
		<div class="insta__images">
			<?php
				$insta = [
					'https://www.instagram.com/p/BI0fohmApkM/', 'https://www.instagram.com/p/BKv-E_djtld/',
					'https://www.instagram.com/p/BNhByiAAXZl/', 'https://www.instagram.com/p/BPBSONQBLX2/'
				];
				for ($i=0; $i < count($insta); $i++) {
					$nr = $i + 1;
					echo '<a href="' . $insta[$i] . '" class="insta__link" target="_blank">';
						echo '<picture class="insta__picture">';
						  echo '<source type="image/webp" srcset="' . ROOT . 'assets/img/insta-image-' . $nr . '.webp">';
						  echo '<img src="' . ROOT . 'assets/img/insta-image-' . $nr . '.jpg" class="insta__picture" alt="#dokgent instagram">';
						echo '</picture>';
					echo '</a>';
				};
			?>
		</div>
	</section>


	<section class="home__events">
		<h2 class="title title_type_big title_color_white">events.</h2>
		<div class="home__events__content">
			<?php foreach ($top_events as $event) {
				echo '<a class="home__events__event-article" href="' . ROOT . 'events/' . $event['id'] .'">';
					echo '<article class="event-article">';
						echo '<picture class="event-article__image event-article__image_type_narrow">';
						  echo '<source type="image/webp" srcset="' . ROOT . 'assets/img/events/' . $event['image'] . '.webp">';
						  echo '<img src="' . ROOT . 'assets/img/events/' . $event['image'] . '.jpg" class="event-article__image event-article__image_type_narrow" alt="' . $event['title'] . '">';
						echo '</picture>';
						echo '<div class="event-article__content">';
							echo '<h3 class="title event-article__title">' . $event['title'] . '</h3>';
							echo '<div class="event-article__tags tags">';
							foreach ($event['locations'] as $location) {
								echo '<span class="tag tag_color_' . $location['color'] . '">' . $location['name'] . '</span>';
							}
							echo '</div>';
							echo '<p class="event-article__paragraph">' . $event['preview'] . '</p>';
						echo '</div>';
					echo '</article>';
				echo '</a>';
			} ?>
		</div>
		<a href="<?php echo ROOT ?>events" class="button button_color_orange">bekijk alle events</a>
	</section>

	<section class="home__zones">
		<h2 class="title title_type_big title_color_white">zones.</h2>
		<div class="home__zones__content">
			<?php
				foreach ($zones as $zone) {
					echo '<a href="' . ROOT . 'zones/dokkantine" class="link_disabled">';
						echo '<article class="article home__zones__article">';
							echo '<h3 class="title article__title">' . $zone['title'] . '</h3>';
							echo '<p class="article__paragraph">' . $zone['preview'] . '</p>';
						echo '</article>';
					echo '</a>';
				}
				if (!is_int(count($zones) / 3)) {
					echo '<article class="article home__zones__article"></article>';
				}
			?>
		</div>
		<a href="<?php echo ROOT ?>zones" class="button button_color_yellow link_disabled">ontdek alle zones</a>
	</section>


	<section class="home__blog">
		<h2 class="title title_type_big title_color_white">blog.</h2>
		<div class="home__blog__content">
			<?php
				$classes = ['left', 'center', 'right'];
				foreach ($blog_posts as $post) {
					echo '<a href="' . ROOT . 'blog/' . $post['id'] . '" class="link_disabled">';
						echo '<article class="article home__blog__article">';
							echo '<h3 class="title article__title">' . $post['title'] . '</h3>';
							echo '<p class="article__paragraph">' . $post['preview'] . '</p>';
						echo '</article>';
					echo '</a>';
				}
			?>
		</div>
		<a href="<?php echo ROOT ?>blog" class="button button_color_green link_disabled">bekijk alle blog posts</a>
	</section>


	<?php require_once WWW_ROOT . 'view' . DS . 'include' . DS . 'footer.php' ?>
