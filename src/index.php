<?php

// testing purpouses only
ini_set('display_errors',1);
error_reporting(E_ALL);

session_start();
date_default_timezone_set('Europe/Brussels');

define('DS', DIRECTORY_SEPARATOR);
define('WWW_ROOT', __DIR__ . DS);
define('ROOT', '/');
define('DEV_URL', 'http://127.0.0.1:3000/');

$bundle_main = array(
  'build' => array(
    'css' => 'main',
    'js' => 'main'
  ),
  'dev' => array(
    'js' => 'main'
  )
);

$routes = array(
  'home' => array(
    'name' => 'home',
  	'controller' => 'Dok',
  	'action' => 'home',
    'bundle' => $bundle_main
  ),
  'eventlist' => array(
    'name' => 'eventlist',
    'controller' => 'Dok',
    'action' => 'eventlist',
    'bundle' => $bundle_main
  ),
  'eventdetail' => array(
    'name' => 'eventdetail',
    'controller' => 'Dok',
    'action' => 'eventdetail',
    'bundle' => $bundle_main
  ),
  'push' => array(
    'controller' => 'Dok',
    'action' => 'push'
  ),
  'get' => array(
    'controller' => 'Dok',
    'action' => 'get'
  )
);

if(empty($_GET['page'])) {
  $_GET['page'] = 'home';
}

if(empty($routes[$_GET['page']])) {
  header('Location: index.php');
  exit();
}

$route = $routes[$_GET['page']];
$controllerName = $route['controller'] . 'Controller';

require_once WWW_ROOT . 'controller' . DS . $controllerName . ".php";

$controllerObj = new $controllerName();
$controllerObj->route = $route;
$controllerObj->filter();
if (isset($route['name']) && isset($route['bundle'])) {
  $controllerObj->render($route);
} else {
  $controllerObj->render();
}

?>
